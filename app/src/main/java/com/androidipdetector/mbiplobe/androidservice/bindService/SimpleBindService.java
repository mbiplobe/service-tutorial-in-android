package com.androidipdetector.mbiplobe.androidservice.bindService;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

/**
 * Created by mbiplobe on 10/15/2015.
 */
public class SimpleBindService extends IntentService {

    public  final IBinder mBinder=new LocalBinder();

    public SimpleBindService() {
        super("dhaka");


    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(getApplicationContext(), "Service are Created", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(), "Service are Started", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }




    public int determineFactorials(int x){
        int fact=1;
        for(int i=1;i<=x;i++){
           fact*=i;
        }
        return fact;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(), "Service are Destroyed", Toast.LENGTH_SHORT).show();
    }

    public class LocalBinder extends Binder{

        public SimpleBindService getService(){

            return SimpleBindService.this;
        }
    }

}
