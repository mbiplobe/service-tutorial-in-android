package com.androidipdetector.mbiplobe.androidservice.bindService;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.androidipdetector.mbiplobe.androidservice.bindService.SimpleBindService.LocalBinder;

import com.androidipdetector.mbiplobe.androidservice.R;

public class BindServiceActivity extends ActionBarActivity {

    private Button bindService,unBindService,determineFactorialButton;
    TextView factorialTextView;
    EditText numberEditText;
    boolean status;
    private SimpleBindService simpleBindService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_service);

        bindService= (Button) findViewById(R.id.bindService);
        unBindService= (Button) findViewById(R.id.unBindService);
        determineFactorialButton= (Button) findViewById(R.id.determineFactorialButton);
        numberEditText= (EditText) findViewById(R.id.numberEditText);
        factorialTextView= (TextView) findViewById(R.id.factorialTextView);

        bindService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(BindServiceActivity.this,SimpleBindService.class);
                bindService(intent,serviceConnection, Context.BIND_AUTO_CREATE);
                status=true;

                Toast.makeText(getApplicationContext(),"Service are now Binded",Toast.LENGTH_SHORT).show();
            }
        });

        unBindService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (status) {
                    unbindService(serviceConnection);
                    Toast.makeText(getApplicationContext(), "Service are now unBinded", Toast.LENGTH_SHORT).show();
                    status = false;
                }
                else{
                    Toast.makeText(getApplicationContext(), "Service are already unBinded", Toast.LENGTH_SHORT).show();
                }
            }
        });

        determineFactorialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status) {
                    int number = Integer.parseInt(numberEditText.getText().toString());
                    factorialTextView.setText(String.valueOf(simpleBindService.determineFactorials(number)));
                }
            }
        });


    }

    public ServiceConnection serviceConnection=new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocalBinder localBinder= (LocalBinder) service;
            simpleBindService=localBinder.getService();
            status=true;

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            status=false;
        }
    };

}
