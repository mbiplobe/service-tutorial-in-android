package com.androidipdetector.mbiplobe.androidservice.normal;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by mbiplobe on 10/14/2015.
 */
public class NormalService extends Service {

    private String wellComeNote;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        wellComeNote=intent.getStringExtra("name");

        Toast.makeText(this,"Service Are Started",Toast.LENGTH_SHORT).show();
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this,"Service Are Created",Toast.LENGTH_SHORT).show();
        stopSelf();
    }

    @Override
    public void onDestroy() {
        Log.d("Service Status", "Service Stop");

        Toast.makeText(this,"Service Are Destroyed",Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}
