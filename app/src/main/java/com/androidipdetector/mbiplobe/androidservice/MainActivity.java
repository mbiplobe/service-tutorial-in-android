package com.androidipdetector.mbiplobe.androidservice;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.androidipdetector.mbiplobe.androidservice.bindService.BindServiceActivity;
import com.androidipdetector.mbiplobe.androidservice.normal.NormalService;


public class MainActivity extends ActionBarActivity {

    private Button normalServiceButton,stopServiceButton,basicIntentService,bindServiceButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        normalServiceButton= (Button) findViewById(R.id.normalServiceButton);
        stopServiceButton= (Button) findViewById(R.id.stopServiceButton);
        basicIntentService= (Button) findViewById(R.id.basicIntentServiceButton);
        bindServiceButton= (Button) findViewById(R.id.bindServiceButton);


        normalServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, NormalService.class);
                intent.putExtra("name","Hello Bangladesh");
                startService(intent);
            }
        });

        stopServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, NormalService.class);
                stopService(intent);
            }
        });

        basicIntentService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NormalService.class);
                intent.putExtra("Writter","Mujahid Islam Biplobe");
                startService(intent);
            }
        });


        bindServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BindServiceActivity.class);
                startActivity(intent);
            }
        });
    }




}
